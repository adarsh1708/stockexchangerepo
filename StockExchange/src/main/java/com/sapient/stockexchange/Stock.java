package com.sapient.stockexchange;

public class Stock {
	private String stockId;

	public Stock(String string) {
		super();
		this.stockId = string;
	}

	public String getStockId() {
		return stockId;
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

}
