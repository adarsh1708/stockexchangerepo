package com.sapient.stockexchange;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.sapient.ordermanagement.Order;
import com.sapient.ordermanagement.OrderBook;
import com.sapient.ordermanagement.Transaction;

public class Market {
	private static Map<Stock, OrderBook> stockOrderBook = new HashMap<>();
    private static List<Transaction> transactions = new LinkedList<>();


    public void addOrder(Order order) {
        Stock stock = order.getStock();
        if(!stockOrderBook.containsKey(stock)) {
            stockOrderBook.put(stock, new OrderBook(stock));
        }

        List<Transaction> transactionsExecuted = stockOrderBook.get(stock).addOrder(order);
        transactions.addAll(transactionsExecuted);
    }

    public void matchOrders(List<Order> orders) {
        try {
			orders.forEach(this::addOrder);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

    public static List<Transaction> getExecutedTransactions() {
        return transactions;
    }

}		
