package com.sapient.trader;

public enum TraderType {
	BUYER, SELLER;
}
