package com.sapient.trader;

public class TraderRegistration {

	private String traderName;
	private TraderAddress address;
	private String traderId;
	private String phoneNo;
	private TraderType type;

	public TraderRegistration(String traderName, TraderAddress address, String phoneNo, String traderId,
			TraderType type) {
		super();
		this.traderName = traderName;
		this.address = address;
		this.phoneNo = phoneNo;
		this.traderId = traderId;
		this.type = type;
	}

	public TraderType getType() {
		return type;
	}

	public void setType(TraderType type) {
		this.type = type;
	}

	public String getTraderName() {
		return traderName;
	}

	public void setTraderName(String traderName) {
		this.traderName = traderName;
	}

	public TraderAddress getAddress() {
		return address;
	}

	public void setAddress(TraderAddress address) {
		this.address = address;
	}

	public String getTraderId() {

		return traderId;
	}

	public void setTraderId(String traderId) {
		this.traderId = traderId;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

}
