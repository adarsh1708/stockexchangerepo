package com.sapient.ordermanagement;

import com.sapient.stockexchange.Stock;

public class Order {

	private Long orderId;
	private Stock stock;
	private OrderType orderType;
	private double price;
	private int quantity;

	public Order(Long id, Stock stock, OrderType orderType, double price, int quantity) {
		this.orderId = id;
		this.stock = stock;
		this.orderType = orderType;
		this.price = price;
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object a) {
		if (!(a instanceof Order)) {
			return false;
		}
		return ((Order) a).orderId.equals(this.orderId);
	}

	public String toString() {
		return orderId + " " + quantity;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
