package com.sapient.ordermanagement;

import com.sapient.stockexchange.Stock;

public class OrderFactory {
	public static Order createOrder(Stock stock, String[] input) throws Exception {
        Long orderId = Long.parseLong(input[0].split("#")[1]);
        OrderType orderType = OrderType.getOrderType(input[2]);
        if(orderType == null) {
            throw new Exception(String.format("Invalid order type::%s", input[2]));
        }
        return new Order(orderId, stock, orderType, Double.parseDouble(input[3]), Integer.parseInt(input[4]));
    }
}
