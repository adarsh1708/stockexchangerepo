package com.sapient.ordermanagement;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.sapient.stockexchange.Stock;
import com.sapient.utils.Constants;

public class OrderBook {
	private Stock stock;
	private static TreeMap<Long, List<Order>> buyOrders = new TreeMap<>();
	private static TreeMap<Long, List<Order>> sellOrders = new TreeMap<>();
	
	public OrderBook(Stock stock) {
        this.stock = stock;
    }

	// to get the orderType of the Order
	public static TreeMap<Long, List<Order>> getOrdersMap(OrderType orderType) {
		if (orderType.equals(OrderType.BUY)) {
			return buyOrders;
		} else {
			return sellOrders;
		}
	}

	// Adding an Order
	public List<Transaction> addOrder(Order order) {
		List<Transaction> transactions = match(order);
		if (order.getQuantity() > 0) {
			insert(order);
		}
		return transactions;
	}
	
	//Creating an Order
	public static Order createOrder(Stock stock, String[] input) throws Exception {
        Long orderId = Long.parseLong(input[0].split("#")[1]);
        OrderType orderType = OrderType.getOrderType(input[3]);
        if(orderType == null) {
            throw new Exception(String.format("Invalid order type::%s", input[3]));
        }
        return new Order(orderId, stock, orderType, Double.parseDouble(input[4]), Integer.parseInt(input[5]));
    }
	
	// to match the orders of a particular trader
	private static List<Transaction> match(Order order) {
		List<Order> candidateOrders = computeCandidateOrders(order);
		List<Transaction> transactions = new LinkedList<>();
		if (!candidateOrders.isEmpty()) {
			Iterator<Order> candidateOrderIterator = candidateOrders.iterator();
			while (order.getQuantity() > 0 && candidateOrderIterator.hasNext()) {
				Order candidateOrder = candidateOrderIterator.next();

				Transaction transaction = computeTransaction(order, candidateOrder);
				transactions.add(transaction);

				if (candidateOrder.getQuantity() == 0) {
					OrderBook.removeOrder(candidateOrder);
				}
			}
		}
		return transactions;
	}

	// for updating the quantity of stocks after the order is matched
	private static Transaction computeTransaction(Order order, Order candidateOrder) {
		int quantityExecuted = Math.min(order.getQuantity(), candidateOrder.getQuantity());

		Order buyOrder;
		Order sellOrder;
		if (order.getOrderType().equals(OrderType.BUY)) {
			buyOrder = order;
			sellOrder = candidateOrder;
		} else {
			buyOrder = candidateOrder;
			sellOrder = order;
		}

		candidateOrder.setQuantity(candidateOrder.getQuantity() - quantityExecuted);
		order.setQuantity(order.getQuantity() - quantityExecuted);

		return new Transaction(buyOrder.getOrderId(), sellOrder.getOrderId(), sellOrder.getPrice(), quantityExecuted);
	}

	// for getting the List of Buy orders if the trader is Seller and Sell Orders if
	// the trader is Buyer
	private static List<Order> computeCandidateOrders(Order order) {
		TreeMap<Long, List<Order>> ordersMap = getOppositeTypeOrderMap(order.getOrderType());
		List<Long> candidateOrdersKeySet = computeCandidateOrdersKeys(ordersMap, order);
		return candidateOrdersKeySet.stream().map(ordersMap::get).flatMap(Collection::stream)
				.collect(Collectors.toList());
	}

	// to get the opposite Side of the trader
	private static TreeMap<Long, List<Order>> getOppositeTypeOrderMap(OrderType orderType) {
		return getOrdersMap(OrderType.getOpposite(orderType));
	}

	// to get the trader Prices(Candidates) as Order Keys
	private static List<Long> computeCandidateOrdersKeys(TreeMap<Long, List<Order>> ordersMap, Order order) {
		OrderType orderType = order.getOrderType();
		List<Long> candidateOrdersKeys = null;
		Long key = computeOrderKey(order);

		if (orderType.equals(OrderType.BUY)) {
			candidateOrdersKeys = new LinkedList<>(ordersMap.headMap(key, true).keySet());
		} else {
			candidateOrdersKeys = new LinkedList<>(ordersMap.headMap(key, true).descendingKeySet());
		}
		return candidateOrdersKeys;
	}

	// Inserting the order in the OrderBook
	public static void insert(Order order) {
		SortedMap<Long, List<Order>> ordersMap = getOrdersMap(order.getOrderType());
		insertOrder(ordersMap, order);
	}

	public static void insertOrder(SortedMap<Long, List<Order>> ordersMap, Order order) {
		Long key = computeOrderKey(order);
		if (!ordersMap.containsKey(key)) {
			ordersMap.put(key, new LinkedList<>());
		}
		ordersMap.get(key).add(order);
	}

	// Removing the Order
	public static void removeOrder(Order order) {
		Long key = computeOrderKey(order);
		SortedMap<Long, List<Order>> ordersMap = getOrdersMap(order.getOrderType());

		if (ordersMap.containsKey(key)) {
			ordersMap.get(key).remove(order);
			if (ordersMap.get(key).isEmpty()) {
				ordersMap.remove(key);
			}
		}
	}
	
	// this is done to ensure no problems with using double as key
	// instead, assume two decimal points, multiply by 100 and save as long
	public static Long computeOrderKey(Order order) {
		return (long) (order.getPrice() * Constants.PRICEKEYMULTIPLIER);
	}
}
