package com.sapient.ordermanagement;

import com.sapient.trader.TraderRegistration;

public class TransactionDetails {
	private Order orderInfo;
	private TraderRegistration traderInfo;

	public TransactionDetails(Order order, TraderRegistration trader) {
		this.orderInfo = order;
		this.traderInfo = trader;
	}

	public Order getOrderDetails() {
		return orderInfo;
	}

	public void setOrderDetails(Order orderInfo) {
		this.orderInfo = orderInfo;
	}

	public TraderRegistration getPurchaserDetails() {
		return traderInfo;
	}

	public void setPurchaserDetails(TraderRegistration purchaserDetails) {
		this.traderInfo = purchaserDetails;
	}

}
