package com.sapient.services;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class LineParser {
	public List<String> parse(String line){
    	List<String> input = new LinkedList<>();
    	
    	Scanner sc = new Scanner(line);
    	
        while (sc.hasNextLine()) {
            input.add(sc.nextLine());
        }
        sc.close();
        
        return input;
    }

}
