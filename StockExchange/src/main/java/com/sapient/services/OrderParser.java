package com.sapient.services;

import java.util.ArrayList;
import java.util.List;

import com.sapient.ordermanagement.Order;
import com.sapient.ordermanagement.OrderFactory;
import com.sapient.stockexchange.Stock;
import com.sapient.utils.Util;

public class OrderParser {
	private StockDatabase stockDatabase = new StockDatabase();
    private List<Order> orders;
    private static final int expectedColumns = 5;
    
    public void parse(String line) throws Exception {
        List<String> orderLines = new LineParser().parse(line);
        List<Order> list = new ArrayList<>();
        for (String inputLine : orderLines) {
            Order order = parseLine(inputLine);
            list.add(order);
        }
        orders = list;
    }
    
    public List<Order> getOrders() {
        return orders;
    }

    private Order parseLine(String orderInput) throws Exception {
        String[] input = Util.getTokens(orderInput, " ", expectedColumns);

        String stockName = input[2];
        stockDatabase.addStock(stockName);
        Stock stock = stockDatabase.getStock(stockName);

        return OrderFactory.createOrder(stock, input);
    }
}
