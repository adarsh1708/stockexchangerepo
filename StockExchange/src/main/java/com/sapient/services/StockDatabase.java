package com.sapient.services;

import java.util.HashMap;
import java.util.Map;

import com.sapient.stockexchange.Stock;

public class StockDatabase {
	private Map<String, Stock> stockMap = new HashMap<>();

    public void addStock(String stockName) {
        if(!stockMap.containsKey(stockName)) {
            Stock stock = new Stock(stockName);
            stockMap.put(stockName, stock);
        }
    }

    public Stock getStock(String stockName) {
        return stockMap.get(stockName);
    }
}
