package com.sapient.client;

import java.util.List;
import java.util.stream.Collectors;

import com.sapient.ordermanagement.Order;
import com.sapient.ordermanagement.Transaction;
import com.sapient.services.OrderParser;
import com.sapient.stockexchange.Market;

public class StockOrderClient {
	public static void main(String[] args) {
		
//		Stock stock1 = new Stock("RLNC101");
//		Stock stock2 = new Stock("AMZN971");
//		Stock stock3 = new Stock("INFY267");
//		
//		Order order1 =new Order(1l, stock1 , OrderType.SELL, 100, 500);
//		Order order2 =new Order(2l, stock2 , OrderType.SELL, 80, 200);
//		Order order3 =new Order(3l, stock3 , OrderType.BUY, 140, 110);
//		
//		List<Order> orderList = new ArrayList<>();
//		orderList.add(order1);
//		orderList.add(order2);
//		orderList.add(order3);
//		
//		System.out.println(orderList);
		
//		Market market = new Market();
//		market.addOrder(order1);
//		market.addOrder(order2);
//		market.addOrder(order3);
		
		String order1 = "#1 BAC sell 240 100\n#2 BAC sell 237.45 90\n#3 BAC buy 238.10 110";
		
		List<String> result = computeTransactionResultsFromOrdersFile(order1);
        result.forEach(System.out::println);
        
	}
	
	public static List<String> computeTransactionResultsFromOrdersFile(String orderLine) {
        OrderParser parser = null;
        try {
            parser = new OrderParser();
            parser.parse(orderLine);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

        Market stockExchange = new Market();
        List<Order> orders = parser.getOrders();
        stockExchange.matchOrders(orders);
        List<Transaction> transactions = Market.getExecutedTransactions();

        return transactions.stream().map(Transaction::toString).collect(Collectors.toList());
    }

}
