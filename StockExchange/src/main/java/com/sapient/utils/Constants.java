package com.sapient.utils;

public class Constants {
	private static final int PRECISION = 2;
	public static final int PRICEKEYMULTIPLIER = (int) Math.pow(10, PRECISION);

	// private constructor to hide the implicit public one
	private Constants() {
		super();
	}

}
