//package com.sapient.stockexchange.ordermanagementTests;
//
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//
//import org.junit.Test;
//
//import com.sapient.ordermanagement.Order;
//import com.sapient.ordermanagement.OrderBook;
//import com.sapient.ordermanagement.OrderType;
//import com.sapient.stockexchange.Stock;
//
//public class VoidMethodTest {
//
//	@Test // test to check whether the transaction details passing through addOrder or not.
//	public void testAddOrder() {
//		OrderBook orderBook = mock(OrderBook.class);
//		Stock stock = new Stock("1100OID");
//		Order order1 = new Order(1011l, stock, OrderType.BUY, 100.0, 100);
//
//		orderBook.addOrder(order1);
//
//		verify(orderBook, times(1)).addOrder(order1);
//	}
//
//	@Test // test to check whether the transaction details passing through deleteOrder or not.
//	public void testDeleteOrder() {
//		OrderBook orderBook = mock(OrderBook.class);
//		Stock stock = new Stock("1100OID");
//		Order order1 = new Order(1011l, stock, OrderType.BUY, 100.0, 100);
//
//		OrderBook.removeOrder(order1);
//
//		verify(orderBook, times(1));
//		OrderBook.removeOrder(order1);
//	}
//}
